function getHtmlMenuAccessible() {
  let main = document.querySelector("main");
  main.innerHTML += `        
    <div id="containerMenuAccessible">
        <button id="ButtonMenuAccessible"><a href="#" id="skipMenuAccessible">
        Menu accessibilite</a></button>
        <div id="menuAccessible">
            <section class="category">
                <h2 class="tittleMenuAccessible">Profils proposés</h2>
                <div class="containerSousCategory">
                    <button class="btnModified">Dyslexique</button>
                    <button class="btnModified">Epileptique</button>
                    <button class="btnModified">TDAH</button>
                    <button class="btnModified">Malvoyant</button>
                    <button class="btnModified">Epileptique</button>
                </div>
            </section>
            <section class="category">
                <h2 class="tittleMenuAccessible">Personnaliser la police</h2>
                <div class="containerSousCategory">
                    <button id="closeMenu">X</button>
                    <button class="btnModified" id="changePoliceDefault">Reset</button>
                    <button class="btnModified" id="changeDyslexique">Dyslexique</button>
                    <button class="btnModified" id="changeDyslexique">Nom police 2</button>
                    <button class="btnModified" id="changeDyslexique">nom police 3</button>
                </div>
            </section>
            <section class="category">
                <h3 class="tittleMenuAccessible">Changer l'alignement du texte</h3>
                <div class="containerSousCategory">
                    <button class="btnModified" id="changeTextAlignDefault">Reset</button>
                    <button class="btnModified" id="textAlignLeft">Gauche</button>
                    <button class="btnModified" id="textAlignCenter">Centre</button>
                    <button class="btnModified" id="textAlignRight">Droite</button>
                </div>
            </section>

            <section class="containerA">
                <h3 class="tittleMenuAccessible">Espacement interligne</h3>
                <div class="containerB">
                    <p class="informationsButtonSecondary">Current value</p>
                    <div class="containerC">
                        <button class="btnModifiedBisL" id="incrementLineHeight"><i
                                class="fa-solid fa-circle-plus">+</i></button>
                        <button class="btnModifiedBisR" id="decrementLineHeight"><i
                                class="fa-solid fa-circle-minus">-</i></button>
                    </div>
                </div>
            </section>

            <section class="containerA">
                <h3 class="tittleMenuAccessible">Taille du texte</h3>
                <div class="containerB">
                    <p class="informationsButtonSecondary">Current value</p>
                    <div class="containerC">
                        <button class="btnModifiedBisL" id="increaseFont"><i
                                class="fa-solid fa-circle-plus">+</i></button>
                        <button class="btnModifiedBisR" id="decreaseFont"><i
                                class="fa-solid fa-circle-minus">-</i></button>
                    </div>
                </div>
            </section>

            <section class="containerA">
                <h3 class="tittleMenuAccessible">Espacement des lettres</h3>
                <div class="containerB">
                    <p class="informationsButtonSecondary">Current value</p>
                    <div class="containerC">
                        <button class="btnModifiedBisL" id="increaseSpace"><i
                                class="fa-solid fa-circle-plus">+</i></button>
                        <button class="btnModifiedBisR" id="decreaseSpace"><i
                                class="fa-solid fa-circle-minus">-</i></button>
                    </div>
                </div>
            </section>
            <section class="category">
                <h3 class="tittleMenuAccessible">Personnaliser la couleur</h3>
                <div class="containerSousCategory">
                    <button class="btnModified" id="colorDefault"> color default</button>
                    <button class="btnModified" id="Monochrome">Monochrome</button>
                    <button class="btnModified" id="color50"> 50%</button>
                </div>
            </section>
            <section>
                <h3 class="tittleMenuAccessible">Aide à la navigation</h3>
                <div class="containerSousCategory">
                    <button class="btnModified" id="highlightTittles"> surligner titre</button>
                    <button class="btnModified" id="highlightLinks"> surligner liens</button>
                    <button class="btnModified" id="deletePictures"> suprimer les images</button>
                </div>
            </section>
        </div>
    </div>
`;
  return;
}
