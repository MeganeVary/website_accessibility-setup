# accessibility-setup

![This is an image](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Disability_symbols.svg/816px-Disability_symbols.svg.png)

## About the project:

Contribute to making navigation more accessible by facilitating implementation on web projects.
Any help is welcome ;)

## Quick start

### To run this project you can :
 
#### Install it locally using npm:

> $ npm install accessibility-setup

__Then in your file you can add this line :__

> require('accessibility-setup/[_file_]')


#### If you want to deleted the package you can in your terminal :

> $ npm remove accessibility-setup

#### Or you can use ```git```

- Go to the [repository accessibility-setup](https://gitlab.com/MeganeVary/accessibility-setup.git)

- Then git clone the project
![imageHowToClone](https://zupimages.net/up/21/48/7fzg.png)

### Documentation :

## To use accessibility-setup :
```

```
## To make a great accessible website :

- Notion with [Accessibility tips ](https://helix-study-c05.notion.site/EasyAccessibility-bffadc71e55542a2a56d5e04e13f3de9)

# Community :
__To join__ :
- [Slack ](https://join.slack.com/t/accessibilityfriendly/shared_invite/zt-zuj149fk-YUxVhzdZIJ5S28D_0DRJYQ)
- [insta]
- [Dribble]
- [YoutubeChannel](https://www.youtube.com/channel/UCm2kb75hX1oi9-C84ZGRlZw)

# My Chanel Youtube about digital accessibility :

## Youtube Channel whose project is to share about digital accessibility.

### __In the program :__

- Intervention of people with disabilities to __have their opinions on website :__
    - __(the differents fonts, colors and contrast,navigation with screen readers etc ...)__


- __Basic Lsf__ (Langue des signes française) __and__ vocabulary for people who work __for the web__

- Some tutorials, introduce the npm "easyaccessibility" library __and other tools__

__And much more ! :)__

- Youtube channel : [hedera_digitalis](https://www.youtube.com/channel/UCm2kb75hX1oi9-C84ZGRlZw)

## If you have questions :

If you have any questions, or if you want to discuss about the project with me or if you want to help me

You can __contact me__ at this email :

- __hedera_digitalis@outlook.fr__
